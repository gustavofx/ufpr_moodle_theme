<?php

/**
* Strings for component 'theme_ufpr', language 'pt_br'
* @package   theme_ufpr
* @author    CIPEAD
* @copyright 2018 CIPEAD UFPR
*/

defined('MOODLE_INTERNAL') || die();

user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);

require_once($CFG->libdir . '/behat/lib.php');

$extraclasses = [];

if (isloggedin()) {
    $extraclasses = ['ufpr-bg'];
    if(!isguestuser()){
        $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
    }
} else {
    $navdraweropen = false;
    $extraclasses = ['ufpr-frontpage'];
}

if ($navdraweropen) {
    $extraclasses[] = 'drawer-open-left';
}

if($PAGE->pagelayout == "coursecategory"){
    require( $CFG->dirroot . "/theme/ufpr/classes/util/ufpr_functions.php");
    $extraclasses[] = theme_ufpr\util\ufpr_functions::get_cat_bg($PAGE->category->path);
};


$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$blockshtml = $OUTPUT->blocks('side-pre');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();
$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'hasblocks' => $hasblocks,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => $navdraweropen,
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu)
];

$templatecontext['flatnavigation'] = $PAGE->flatnav;
$templatecontext['image_cipead'] = $OUTPUT->image_url('cipead', 'theme');
$templatecontext['is_unloged'] = !isloggedin();

if (isloggedin() and isset($CFG->frontpageloggedin)) {    
    echo $OUTPUT->render_from_template('theme_ufpr/frontpage', $templatecontext);    
} else {    
    echo $OUTPUT->render_from_template('theme_ufpr/frontpage_unloged', $templatecontext);    
}



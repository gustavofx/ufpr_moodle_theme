<?php
/**
 * Language file.
 *
 * @package   theme_ufpr
 * @copyright 2018 Gustavo Rudiger
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'UFPR';
$string['configtitle'] = 'UFPR';
$string['choosereadme'] = 'UFPR é um tema filho de Boost';

// General settings tab.
$string['generalsettings'] = 'Geral';
$string['preset'] = 'Preset do tema';
$string['preset_desc'] = 'Escolha um preset para alterar a aparência do tema.';
$string['presetfiles'] = 'Arquivos de preset adicionais';
$string['presetfiles_desc'] = 'Arquivos preset podem ser usados para alterar dramaticamente a aparência do tema. Veja <a href=https://docs.moodle.org/dev/Boost_Presets>Boost presets</a> para informações sobre criar e compartilhar seus próprios arquivos preset, e veja o <a href=http://moodle.net/boost>repositório de presets</a> que outros compartilharam.';
$string['brandcolor'] = 'Cor da marca';
$string['brandcolor_desc'] = 'Cor principal.';

 // Advanced settings tab.
$string['advancedsettings'] = 'Avançado';
$string['rawscsspre'] = 'SCSS inicial puro';
$string['rawscsspre_desc'] = 'Neste campo você pode fornecer código SCSS de inicialização, ele será injetado antes de tudo. Na maioria dos casos você usará esta configuração para setar variáveis.';
$string['rawscss'] = 'SCSS puro';
$string['rawscss_desc'] = 'Use este campo para fornecer código SCSS ou CSS que será injetado no final do arquivos de estilos.';

// Navbar settings tab
$string['navbarsettings'] = "Navbar";

$string['mural'] = "Mural";
$string['categories'] = "Categorias";

$string['enter-course-name'] = "Digite o nome do curso";
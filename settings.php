<?php
 
// Every file should have GPL and copyright in the header - we skip it in tutorials but you should not skip it for real.
 
// This line protects the file from being accessed by a URL directly.                                                               
defined('MOODLE_INTERNAL') || die();                                                                                                
 
// This is used for performance, we don't need to know about these settings on every page in Moodle, only when                      
// we are looking at the admin settings pages.                                                                                      
if ($ADMIN->fulltree) {                                                                                                             
 
    // Boost provides a nice setting page which splits settings onto separate tabs. We want to use it here.                         
    $settings = new theme_boost_admin_settingspage_tabs('themesettingufpr', get_string('configtitle', 'theme_ufpr'));             
 
    // Each page is a tab - the first is the "General" tab.                                                                         
    $page = new admin_settingpage('theme_ufpr_general', get_string('generalsettings', 'theme_ufpr'));                             
 
    // Replicate the preset setting from boost.                                                                                     
    $name = 'theme_ufpr/preset';                                                                                                   
    $title = get_string('preset', 'theme_ufpr');                                                                                   
    $description = get_string('preset_desc', 'theme_ufpr');                                                                        
    $default = 'default.scss';                                                                                                      
 
    // We list files in our own file area to add to the drop down. We will provide our own function to                              
    // load all the presets from the correct paths.                                                                                 
    $context = context_system::instance();                                                                                          
    $fs = get_file_storage();                                                                                                       
    $files = $fs->get_area_files($context->id, 'theme_ufpr', 'preset', 0, 'itemid, filepath, filename', false);                    
 
    $choices = [];                                                                                                                  
    foreach ($files as $file) {                                                                                                     
        $choices[$file->get_filename()] = $file->get_filename();                                                                    
    }                                                                                                                               
    // These are the built in presets from Boost.                                                                                   
    $choices['default.scss'] = 'default.scss';                                                                                      
    $choices['plain.scss'] = 'plain.scss';                                                                                          
 
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);                                     
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                        
    $page->add($setting);                                                                                                           
 
    // Preset files setting.                                                                                                        
    $name = 'theme_ufpr/presetfiles';                                                                                              
    $title = get_string('presetfiles','theme_ufpr');                                                                               
    $description = get_string('presetfiles_desc', 'theme_ufpr');                                                                   
 
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'preset', 0,                                         
        array('maxfiles' => 20, 'accepted_types' => array('.scss')));                                                               
    $page->add($setting);     
 
    // Variable $brand-color.                                                                                                       
    // We use an empty default value because the default colour should come from the preset.                                        
    $name = 'theme_ufpr/brandcolor';                                                                                               
    $title = get_string('brandcolor', 'theme_ufpr');                                                                               
    $description = get_string('brandcolor_desc', 'theme_ufpr');                                                                    
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '');                                               
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                        
    $page->add($setting);                                                                                                           
 
    // Must add the page after definiting all the settings!                                                                         
    $settings->add($page);                                                                                                          
 
    // Advanced settings.                                                                                                           
    $page = new admin_settingpage('theme_ufpr_advanced', get_string('advancedsettings', 'theme_ufpr'));                           
 
    // Raw SCSS to include before the content.                                                                                      
    $setting = new admin_setting_configtextarea('theme_ufpr/scsspre',                                                              
        get_string('rawscsspre', 'theme_ufpr'), get_string('rawscsspre_desc', 'theme_ufpr'), '', PARAM_RAW);                      
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                        
    $page->add($setting);                                                                                                           
 
    // Raw SCSS to include after the content.                                                                                       
    $setting = new admin_setting_configtextarea('theme_ufpr/scss', get_string('rawscss', 'theme_ufpr'),                           
        get_string('rawscss_desc', 'theme_ufpr'), '', PARAM_RAW);                                                                  
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                        
    $page->add($setting); 
    
    $settings->add($page); 

    // Navbar settings.                                                                                                           
    $page = new admin_settingpage('theme_ufpr_navbar', get_string('navbarsettings', 'theme_ufpr'));   

    // First Category
    $setting = new admin_settings_coursecat_select('theme_ufpr/nav_first_course_cat', get_string('nav_first_course_cat','theme_ufpr'), get_string('nav_course_cat_desc','theme_ufpr'), '');
    $setting->set_updatedcallback('theme_reset_all_caches'); 
    $page->add($setting);

    // First Category Color    
    $setting = new admin_setting_configcolourpicker('theme_ufpr/nav_first_color', get_string('nav_first_color','theme_ufpr'), get_string('nav_color_desc','theme_ufpr'), '#704C99');                                               
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                        
    $page->add($setting);  

    // Second Category
    $setting = new admin_settings_coursecat_select('theme_ufpr/nav_second_course_cat', get_string('nav_second_course_cat','theme_ufpr'), get_string('nav_course_cat_desc','theme_ufpr'), '');
    $setting->set_updatedcallback('theme_reset_all_caches'); 
    $page->add($setting);

    // Second Category Color    
    $setting = new admin_setting_configcolourpicker('theme_ufpr/nav_second_color', get_string('nav_second_color','theme_ufpr'), get_string('nav_color_desc','theme_ufpr'), '#7EAF36');                                               
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                        
    $page->add($setting); 

    // Third Category
    $setting = new admin_settings_coursecat_select('theme_ufpr/nav_third_course_cat', get_string('nav_third_course_cat','theme_ufpr'), get_string('nav_course_cat_desc','theme_ufpr'), '');
    $setting->set_updatedcallback('theme_reset_all_caches'); 
    $page->add($setting);

    // Third Category Color    
    $setting = new admin_setting_configcolourpicker('theme_ufpr/nav_third_color', get_string('nav_third_color','theme_ufpr'), get_string('nav_color_desc','theme_ufpr'), '#F8615D');                                               
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                        
    $page->add($setting); 
 
    $settings->add($page);                                                                                                          
}
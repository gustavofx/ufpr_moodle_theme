<?php
 
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Overriden theme boost core renderer.
 *
 * @package    theme_ufpr
 * @copyright  2018 Gustavo Rudiger
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_ufpr\output;
defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/blocks/myoverview/classes/output/renderer.php');
use block_myoverview\output\main;
use html_writer;

class block_myoverview_renderer extends \block_myoverview\output\renderer {

    /**
     * Return the main content for the block overview.
     *
     * @param main $main The main renderable
     * @return string HTML string
     */
    public function render_main(main $main) {
        $o = $main->export_for_template($this);
 
        global $DB;

        // Faz um vetor onde as chaves são as categorias previamente configuradas
        // para equivaler a classe para coloração adequada
        $cat_class = array (
            get_config('theme_ufpr', 'nav_first_course_cat') =>  "cat-cor-1",
            get_config('theme_ufpr', 'nav_second_course_cat') => "cat-cor-2",
            get_config('theme_ufpr', 'nav_third_course_cat') =>  "cat-cor-3"
        );

        $courses = $o['coursesview']['inprogress']['pages'][0]['courses'];
        foreach ($courses as $course) {
            $idcourse = $course->id;
            // print_r($idcourse);
            // echo nl2br (" \n ");
            // echo nl2br (" \n ");
            $courseDB= get_course($idcourse);
            $category = $DB->get_record('course_categories',array('id'=>$courseDB->category));
            $parentcatids = explode("/", $category->path);
            $category = $parentcatids[1];
            // print_r($course);
            // echo nl2br (" \n ");
            // echo nl2br (" \n ");
            $course->{'categoriacorclasse'}=$cat_class[$category];
            // print_r($course);
            // echo nl2br (" \n ");
            // echo nl2br (" \n ");
        }

        // exit;

        return $this->render_from_template('block_myoverview/main',$o );
    }   
}

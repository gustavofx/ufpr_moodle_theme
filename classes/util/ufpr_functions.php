<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mustache helper to load a theme configuration.
 *
 * @package   theme_ufpr
 * @author    CIPEAD
 * @copyright 2018 CIPEAD UFPR
 */

namespace theme_ufpr\util;


defined('MOODLE_INTERNAL') || die();

class ufpr_functions{

    public static function get_cat_bg($category_path){
        global $DB;

        $cat_id_1 = get_config('theme_ufpr', 'nav_first_course_cat');
        $cat_id_2 = get_config('theme_ufpr', 'nav_second_course_cat');
        $cat_id_3 = get_config('theme_ufpr', 'nav_third_course_cat');

        $course_cats = explode("/", $category_path);
        $class = '';

        switch ($course_cats[1]){
            case $cat_id_1:
                $class = 'first-cat';
                break;
            case $cat_id_2:
                $class = 'second-cat';
                break;
            case $cat_id_3:
                $class = 'third-cat';
                break;
            default:
                $class = 'ufpr-bg';
                break;
        }

        return $class;

    }


}